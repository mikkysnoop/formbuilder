<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'ClientController@index')->name('home');
Route::get('/create/clients', 'ClientController@createClient')->name('create_client');
Route::get('/edit/clients', 'ClientController@editClient')->name('edit_client');
Route::get('/form/management', 'FormController@formManagement')->name('form_management');
Route::get('/show/forms', 'FormController@showForms')->name('show_forms');


Route::post('/save/clients', 'ClientController@saveOrUpdateClientDetails')->name('save_new_client');
Route::get('/update/form', 'FormController@updateOrCreateForm')->name('update_form');
Route::post('/add/field', 'FormController@addNewClientField')->name('add_field');

