<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use App\Services\ClientServices;
use App\Services\FormServices;

class ClientController extends Controller
{
    private $clientServices;
    private $formServices;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ClientServices $clientServices, FormServices $formServices)
    {
        $this->middleware('auth');
        $this->clientServices = $clientServices;
        $this->formServices = $formServices;
    }


    /**
     * Show the application dashboard.
     * wth client status count
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $getClients = $this->clientServices->getAllClient();
        return view('home')->with([
            'component' => 'dashboard',
            'clients' => $getClients
        ]);
    }


    /*
     * todo encrypt all urls and separate column identification
     * returns view to create a client
     */
    public function createClient(): Renderable
    {
        $form = $this->clientServices->getNewClientForm();
        return view('home')
            ->with([
                'component' => 'form_create',
                'form' => $form,
                'title' => 'Create New Client',
                'heading' => 'Organisation (Head Office)',
                'route' => 'save_new_client',
            ]);
    }


    public function editClient(Request $request): Renderable
    {
        $formSelected = $request->get('form');
        $clientId = $request->get('client');
        $formDetails = $this->formServices->getFormNameAndTable($request->get('form'));
        $getClientData = $this->clientServices->getClientById($clientId);
        $form = $this->formServices->getFormBySection($formDetails['form_sections_id']);
        $allSections = $this->formServices->getAllClientFormSections();
        return view('home')
            ->with([
                'showButtons' => true,
                'component' => 'form_edit',
                'form' => $form,
                'title' => $getClientData['client_name'],
                'id' => $clientId,
                'clientId' => $clientId,
                'allSections' => $allSections,
                'formSelected' => $formSelected ?? 1,
                'clientData' => $getClientData,
                'heading' => $formDetails['name'],
                'route' => 'save_new_client',
            ]);
    }


    public function saveOrUpdateClientDetails(Request $request): RedirectResponse
    {
        try {
            $status = $this->clientServices->saveOrUpdateClientDetails($request->get('id'), $request->all());
        } catch (\Exception $se) {
            Session::flash('error', $se->getMessage());
            return redirect()->back();
        }
        Session::flash('success', $status);
        return redirect()->back();
    }

}
