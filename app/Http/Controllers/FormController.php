<?php

namespace App\Http\Controllers;

use App\Forms;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Services\FormServices;
use Illuminate\Support\Str;


class FormController extends Controller
{
    private $formServices;


    public function __construct(FormServices $formServices)
    {
        $this->middleware('auth');
        $this->formServices = $formServices;
    }


    public function formManagement(): Renderable
    {
        $getSections = $this->getAllSections();
        return view('home')->with('component', 'form_management_index')
            ->with('group', $getSections);
    }


    public function showForms(Request $request): Renderable
    {
        $getSections = $this->getAllSections();
        $getSelected = DB::table('form_sections')->where('id', $request->get('group_id'))->first();
        $getForms = DB::table('forms')->where('form_sections_id', $getSelected->id)->get();
        return view('home')->with('component', 'form_management_index')->with('group', $getSections)
            ->with('selected', $getSelected)
            ->with('forms', $getForms);
    }


    public function updateOrCreateForm(Request $request){
            if ($request->get('form_id')) {
                $getExistingDetails = Forms::find($request->get('form_id'));
                $message = 'Form record has been successfully updated';
                $getExistingDetails->update($request->all());
            } else {
                $message = 'New form field has been successfully created';
                $client = new Forms;
                $client->create($request->all());
            }
            Session::flash('success', $message);
            return redirect()->back();
    }


    public function addNewClientField(Request $request): RedirectResponse{
        try{
            $request->merge([
                'client_column_name' => Str::snake($request->get('name'))
            ]);
            $this->formServices->addNewField($request->all());
        } catch (\Exception $se){
             DB::rollBack();
            Session::flash('error', $se->getMessage());
            return redirect()->back();
        }
        return redirect()->back();
    }


    private function getAllSections(): object
    {
        return DB::table('form_sections')->get();
    }
}
