<?php


namespace App\Services;


use App\Forms;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

define('BasicInformation', 1);

class FormServices
{
    public function getFormNameAndTable(?int $formId): array
    {
        $formId = $formId ?? BasicInformation;
        $formDetails = [];
        $getSections = $this->getAllClientFormSections();
        foreach ($getSections as $sections) {
            if ($sections->id == $formId) {
                $formDetails['name'] = $sections->name;
                $formDetails['form_sections_id'] = $sections->id;
            }
        }
        return $formDetails;
    }


    public function getAllClientFormSections(): object
    {
        return DB::table('form_sections')->get();
    }


    public function getFormBySection(int $sectionId): object
    {
        $form = DB::table('forms')->where('form_sections_id', $sectionId)->get();
        foreach ($form as $forms) {
            $forms->options ? $forms->options = explode(',', $forms->options) : [];
        }
        return $form;
    }


   /*
    *  add field into the field table
   */
    public function addNewField(array $fieldDetails): bool
    {
        $this->createFieldColumnsOnClientTable($fieldDetails);
        $addField = new Forms;
        return $addField->create($fieldDetails);
    }


    /*
     *  creates the column name on the client table todo move to protected queries
    */
    private function createFieldColumnsOnClientTable($fieldInfo): void
    {
         $columnName = $fieldInfo['client_column_name'];
          if ($fieldInfo['data_type'] == 'text'){
              DB::select( DB::raw("alter table clients add $columnName varchar(200) null") );
          } elseif ($fieldInfo->data_type == 'number'){
              DB::select( DB::raw("alter table clients add $columnName int(11) null") );
          } elseif ($fieldInfo->data_type == 'date'){
              DB::select( DB::raw("alter table clients add $columnName date(11) null") );
          }
    }
}
