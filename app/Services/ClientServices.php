<?php


namespace App\Services;


use App\Clients;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClientServices
{
    public function getAllClient(): object
    {
        return Clients::get();
    }


    public function getNewClientForm(): object
    {
        $form = DB::table('forms')->where('form_sections_id', 1)->get();
        foreach ($form as $forms) {
            $forms->options ? $forms->options = explode(',', $forms->options) : [];
        }
        return $form;
    }


    public function getClientById($clientId){
       return Clients::find($clientId)->toArray();
    }


    public function saveOrUpdateClientDetails(?int $id,array $clientRecord): string{
        if ($id) {
            $getExistingDetails = Clients::find($id);
            $message = 'Client record has been successfully updated';
            $getExistingDetails->update($clientRecord);
        } else {
            $message = 'New client has been successfully created';
            $client = new Clients;
            $client->create($clientRecord);
        }
        return $message;
    }

}
