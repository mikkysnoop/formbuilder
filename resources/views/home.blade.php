@include('head')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    @include('top-menu')
    @include('side-menu')
    <div class="content-wrapper">

    @if(\Illuminate\Support\Facades\Session::has('error'))
    <div class="bg-gradient-danger container-fluid">&nbsp; {{\Illuminate\Support\Facades\Session::get('error')}}</div>
    @endif

    @if(\Illuminate\Support\Facades\Session::has('success'))
    <div class="bg-gradient-success container-fluid">&nbsp;  {{\Illuminate\Support\Facades\Session::get('success')}}</div>
    @endif

    @include('components.'.$component)

    </div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
@include('footer')
</body>

