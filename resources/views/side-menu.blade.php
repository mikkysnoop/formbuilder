<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-gra elevation-4">
    <!-- Brand Logo -->
    <a style="background:#0059b3" href="" class="brand-link">
        <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">CAC House Of Prayer</span>
    </a>

    <!-- Sidebar -->
    <div style="background: white" class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div >
                <a href="#" class="d-block nav-link"><b style="color:#666699">{{\Illuminate\Support\Facades\Auth::user()->name}}</b></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li>
                    @if(\Request::route()->getName() == 'home' )
                        <a  href="{{route('home')}}"  class="nav-link active bg-gradient-lightblue">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p><b style="color:white">All Clients</b></p>
                        </a>
                    @else
                    <a  href="{{route('home')}}"  class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p><b style="color:#666699">All Clients</b></p>
                    </a>
                    @endif
                </li>
            </ul>
        </nav>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li>
                    @if(\Request::route()->getName() == 'create_client' )
                    <a href="{{route('create_client')}}" class="nav-link active bg-gradient-lightblue">
                        <i class="nav-icon fas fa-user"></i>
                        <p><b style="color:white"> Create New Client</b></p>
                    </a>
                    @else
                    <a href="{{route('create_client')}}" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p><b style="color:#666699"> Create New Client</b></p>
                    </a>
                    @endif
                </li>
            </ul>
        </nav>

        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li>
                    @if(\Request::route()->getName() == 'form_management' )
                        <a href="{{route('form_management')}}" class="nav-link active bg-gradient-lightblue">
                            <i class="nav-icon fas fa-user"></i>
                            <p><b style="color:white"> Client Form Management</b></p>
                        </a>
                    @else
                        <a href="{{route('form_management')}}" class="nav-link">
                            <i class="nav-icon fas fa-user"></i>
                            <p><b style="color:#666699"> Client Form Management</b></p>
                        </a>
                    @endif
                </li>
            </ul>
        </nav>


{{--        <nav class="mt-2">--}}
{{--            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">--}}
{{--                <li>--}}
{{--                    <a href="#" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-user"></i>--}}
{{--                        <p> <b style="color:#666699">User Management</b></p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </nav>--}}

{{--        <nav class="mt-2">--}}
{{--            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">--}}
{{--                <li>--}}
{{--                    <a href="#" class="nav-link">--}}
{{--                        <i class="nav-icon fas fa-book"></i>--}}
{{--                        <p> <b style="color:#666699">Management Reports</b></p>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </nav>--}}
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
