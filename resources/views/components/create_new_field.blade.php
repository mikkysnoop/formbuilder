<div class="card-body">
    <form method="post" action="{{route('add_field')}}">
        @csrf
        <table id="example1" class="table table-bordered table-striped table-hover table-sm">
            <thead>
            <tr>
                <td><input required name="name" placeholder="add new field" class="form-control" value=""/></td>
                <td>
                    <select required name="field_type" class="form-control">
                        <option value="text">Text Field</option>
                        <option value="select">Select Option</option>
                    </select>
                </td>
                <td>
                    <select required name="required" class="form-control">
                        <option value="required">Required</option>
                        <option value="not">Not Required</option>
                    </select>
                </td>
                <td>
                    <select required name="data_type" class="form-control">
                        <option value="text">Text</option>
                        <option value="number">Number</option>
                        <option value="date">Date</option>
                    </select>
                </td>
                <td><textarea  style="margin-top: 0px; margin-bottom: 0px; height: 36px;" class="form-control"
                              name="options"></textarea></td>
                <td class="text-center">
                    <button class="btn btn-info btn-sm">Save</button>
                </td>
            </tr>
            <input required name="form_sections_id" hidden value="{{$selected->id}}">
            </thead>
        </table>
    </form>
</div>

<hr/>
