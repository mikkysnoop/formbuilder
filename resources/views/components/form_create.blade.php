<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 style="color: #666699" class="m-0">Clients Onboarding Form</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container-fluid">
    <!-- SELECT2 EXAMPLE -->
    <form method="post" action="{{route($route)}}">
        @csrf

        <div class="card card-default">
            <div class="card-header bg-gradient-lightblue">
                <h3 class="card-title">{{$heading}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @foreach($form as $fields)
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{$fields->name}}</label>
                                @if($fields->field_type == 'text')
                                    <input type="{{$fields->data_type}}"
                                           {{$fields->required}} name="{{Illuminate\Support\Str::snake($fields->name)}}"
                                           class="form-control"/>
                                @elseif($fields->field_type == 'select')
                                    <select name="{{Illuminate\Support\Str::snake($fields->name)}}"
                                            {{$fields->required}} class="form-control select2" style="width: 100%;">
                                        <option></option>
                                        @if($fields->options)
                                            @foreach($fields->options as $options)
                                                <option value="{{$options}}">{{$options}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                @elseif($fields->field_type == 'checkbox')
                                    <input type="checkbox" name="{{Illuminate\Support\Str::snake($fields->name)}}"
                                           class="form-control"/>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <!-- /.card-body -->
            <div class="card-footer">
                <div align="right">
                    <button class="btn btn-info">Save <i class="fas fa-check-circle"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<br/>
<div class="container-fluid">

</div>




