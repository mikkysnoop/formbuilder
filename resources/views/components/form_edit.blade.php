<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 style="color: #666699" class="m-0">{{$title}}</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
        @if($showButtons)
            @foreach($allSections as $key => $sections)
                <a href="{{route('edit_client',['client' => $clientId, 'form' => $sections->id])}}">
                    <button @if($formSelected == $sections->id)
                            class="btn btn-sm bg-gradient-lightblue"
                            @else
                            class="btn btn-sm bg-gradient-gray"
                        @endif >
                        {{$sections->name}}
                    </button>
                </a>
                @if($key == 7)
                    <br/><br/>
                @endif
            @endforeach
            <br/>
        @endif
    </div><!-- /.container-fluid -->
</div>
<div class="container-fluid">
    <form method="post" action="{{route($route)}}">
        @csrf
        <div class="card card-default">
            <div class="card-header bg-gradient-lightblue">
                <h3 class="card-title">{{$heading}}</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @foreach($form as $fields)
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{$fields->name}}</label>
                                @if($fields->field_type == 'text')
                                    <input type="{{$fields->data_type}}" {{$fields->required}}
                                           name="{{$fields->client_column_name}}"
                                           @if($fields->data_type == 'date')
                                           value="{{$clientData[$fields->client_column_name]}}"
                                           @else
                                           value="{{$clientData[$fields->client_column_name]}}"
                                           @endif
                                           class="form-control"/>


                                @elseif($fields->field_type == 'select')
                                    <select name="{{$clientData[$fields->client_column_name]}}"
                                            {{$fields->required}} class="form-control select2" style="width: 100%;">
                                        <option></option>
                                        @if($fields->options)
                                            @foreach($fields->options as $options)
                                                <option
                                                    @if($clientData[$fields->client_column_name] == $options) selected
                                                    @endif value="{{$options}}">{{$options}}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                @elseif($fields->field_type == 'checkbox')
                                    <input type="checkbox" name="{{$clientData[$fields->client_column_name]}}"
                                           class="form-control"/>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <input hidden name="id" value="{{$id}}">
            <!-- /.card-body -->
            <div class="card-footer">
                <div align="right">
                    <button class="btn btn-info">Update <i class="fas fa-check-circle"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<br/>
<div class="container-fluid">

</div>




