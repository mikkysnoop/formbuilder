<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 style="color: #666699" class="m-0">Clients Onboarding Form</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container-fluid">
    <!-- SELECT2 EXAMPLE -->
    <form method="post" action="{{route('save_new_client')}}">
        @csrf
    <div class="card card-default">
        <div class="card-header bg-gradient-lightblue">
            <h3 class="card-title">Basic Information</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Client Name</label>
                        <input required name="client_name" class="form-control"/>
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                            <label>status</label>
                        <select name="status_id" required class="form-control select2" style="width: 100%;">
                            <option></option>
                            @foreach($status as $statuses)
                                <option name="status" value="{{$statuses->id}}">{{$statuses->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Contract Expiry Date </label>
                        <input name="contract_expiry_date" type="date" class="form-control"/>
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label>Business silo</label>
                        <select name="business_silo_id" required class="form-control select2" style="width: 100%;">
                            <option></option>
                            @foreach($businessSilo as $businessSilos)
                                <option name="business_silo" value="{{$businessSilos->id}}">{{$businessSilos->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label>Monthly fee</label>
                        <input name="monthly_fee" required class="form-control"/>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label>NPS score</label>
                        <input name="NPS_score" required class="form-control"/>
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
            <div align="right"><button class="btn btn-info">Save <i class="fas fa-check-circle"></i></button></div>
        </div>
    </div>
    </form>
</div>

<br/>
<div class="container-fluid">

</div>




