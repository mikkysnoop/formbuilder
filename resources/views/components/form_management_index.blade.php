<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 style="color: #666699" class="m-0">Please select a department</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="card">
        <form method="get" action="{{route('show_forms')}}">
            <div class="card-header">
                <div class="row">
                    <div class="col-3">
                        <select class="form-control" name="group_id">
                            <option></option>
                            @foreach($group as $groups)
                            <option @if(isset($selected->id) && $selected->id == $groups->id) selected @endif value="{{$groups->id}}">{{$groups->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-1"><button class="btn btn-info">Search</button></div>
                </div>
            </div>
        </form>
            @if(isset($selected->id))
              @include('components.create_new_field')
            @endif
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover table-sm">
                <thead>
                <tr class="bg-gradient-lightblue">
                    <td>Form Name</td>
                    <td>Input Type</td>
                    <td>status</td>
                    <td>Data Type</td>
                    <td>Options</td>
                    <td class="text-center">Options</td>
                </tr>
                </thead>
                <tbody>
                @if(isset($forms))
                    @foreach($forms as $form)
                        <form method="get" action="{{route('update_form')}}">
                            <tr>
                                <td><input name="name" class="form-control" value="{{$form->name}}" /></td>
                                <td>
                                    <select name="field_type" class="form-control">
                                        <option @if($form->field_type == 'text') selected @endif value="text">Text Field</option>
                                        <option @if($form->field_type == 'select') selected @endif value="select">Select Option</option>
                                    </select>
                                </td>
                            <td>
                                <select name="required" class="form-control">
                                        <option @if($form->required == 'required') selected @endif value="required">Required</option>
                                        <option @if($form->required == 'not required') selected @endif value="not">Not Required</option>
                                </select>
                            </td>
                                <td>
                                    <select name="data_type" class="form-control">
                                        <option @if($form->data_type == 'text') selected @endif value="text">Text</option>
                                        <option @if($form->data_type == 'number') selected @endif value="number">Number</option>
                                        <option @if($form->data_type == 'date') selected @endif value="date">Date</option>
                                    </select>
                                </td>
                                <td><textarea style="margin-top: 0px; margin-bottom: 0px; height: 36px;" class="form-control" name="options">{{$form->options}}</textarea></td>
                                <td class="text-center">
                                    <button class="btn btn-info btn-sm">Update</button>
                                </td>
                            </tr>
                            <input name="form_id" value="{{$form->id}}" hidden>
                    </form>
                   @endforeach
                @endif
            </table>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<!-- /.content -->


