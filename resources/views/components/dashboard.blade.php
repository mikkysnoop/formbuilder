<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 style="color: #666699" class="m-0">Please select a client</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-3"><input placeholder="First Name" class="form-control" /></div>
                <div class="col-1"><button class="btn btn-info">Search</button></div>
            </div>

        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover table-sm">
                <thead>
                <tr class="bg-gradient-lightblue">
                    <td>Id</td>
                    <td>Client Name</td>
                    <td>Status</td>
                    <td>Business Silo</td>
                    <td class="text-center">Options</td>
                </tr>
                </thead>
                <tbody>

                @foreach($clients as $client)
                <tr>
                    <td>{{$client->id}}</td>
                    <td>{{$client->client_name}}</td>
                    <td>{{$client->status}}</td>
                    <td>{{$client->business_silo}}</td>
                    <td class="text-center">
                        <a href="{{route('edit_client',['client' => $client->id])}}"><button class="btn btn-info btn-sm">Select</button></a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <!-- /.card-body -->
    </div>
</section>
<!-- /.content -->


